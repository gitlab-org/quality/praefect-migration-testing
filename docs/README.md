## Praefect Migration Testing 

This project has the sole goal of testing praefect database migrations to ensure that they complete without errors, and also in a timely manner.

## Generating Test Data 
Use a sample praefect db of your choice, or by using some scripts provided in the `./data/scripts` directory.  
With the dataset in place use the following pg_dump command to extract the dataset for use in the tests 

```pg_dump -U [someuser] -Z9 -Fc [some_database] > ./data/[somefolder]/database.tar.gz```

Pay particular attention to the `[somefolder]` element where you dump the data as this is required later.


## Running the tests
Set the environment variables as desired 

```
export DATASET=[somefolder]                     # value matching the dataset you want to test against /data/${DATASET}/database.tar.gz
export POSTGRES_VERSION=12.9                    # desired target postgres version
export PRAEFECT_TARGET_VERSION=14.5.2-ee.0      # desired target version of praefect
```

With the environment variables set you can simply run 

```
docker-compose down --volumes
docker-compose up 

```

Watch the logs of the praefect container, and ensure they complete the migration and begin to run.
In a seperate terminal window, you can check the health status of the praefect container using `docker ps`

We use a timeout on the CI pipeline to ensure that this completes in a timely manner and does not hang during the migration. 

