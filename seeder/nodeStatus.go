package seeder

import (
	"time"
)

func (s *Seed) NodeStatus(n int) error {
	defer s.TimeTrack(time.Now())
	tx := s.DB.MustBegin()
	for i := 0; i < n; i++ {
		praefectName := s.fakePraefectName()
		shardName := s.fakeShardName()
		nodeName := *s.fakeStorage(false)
		if _, found := s.shardNodeNamesOnNodeStatusIdx[praefectName+shardName+nodeName]; found {
			continue
		}
		s.shardNodeNamesOnNodeStatusIdx[praefectName+shardName+nodeName] = true

		tx.MustExec(`
		INSERT INTO node_status (praefect_name, shard_name, node_name, last_contact_attempt_at, last_seen_active_at) 
		VALUES ($1, $2, $3, $4, $5)
		`,
			praefectName,
			shardName,
			nodeName,
			s.fakeTimeStampBetween(time.Now().Add(-31*24*time.Hour), time.Now().Add(365*24*time.Hour)),
			s.fakeTimeStampBetween(time.Now().Add(-31*24*time.Hour), time.Now().Add(365*24*time.Hour)),
		)
	}
	return tx.Commit()
}

func (s *Seed) fakePraefectName() string {
	defer s.TimeTrack(time.Now())
	return randItem([]string{
		"default", "praefect-1", "praefect-2", "praefect-3", "praefect-4", "praefect-5",
	})
}

func (s *Seed) fakeShardName() string {
	defer s.TimeTrack(time.Now())
	return randItem([]string{
		"default", "shard-1", "shard-2", "shard-3", "shard-4", "shard-5",
	})
}
