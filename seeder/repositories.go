package seeder

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"math/rand"
	"time"
)

type repostitory struct {
	VirtualStorage string
	RelativePath   string
	Generation     *int
	Primary        *string
	RepositoryID   int
}

func (s *Seed) Repositories(n int) error {
	defer s.TimeTrack(time.Now())
	tx := s.DB.MustBegin()
	for i := 1; i < n; i++ {
		r := &repostitory{
			VirtualStorage: s.fakeVirtualStorage(),
			RelativePath:   s.fakeRelativePath(),
			Generation:     s.fakeGeneration(),
			Primary:        s.fakeStorage(true),
			RepositoryID:   i,
		}
		tx.MustExec(`
		INSERT INTO repositories (virtual_storage, relative_path, generation, "primary") 
		VALUES ($1, $2, $3, $4)`,
			r.VirtualStorage, r.RelativePath, r.Generation, r.Primary,
		)
		s.cacheRepositories = append(s.cacheRepositories, r)
	}
	return tx.Commit()
}

func (s *Seed) fakeVirtualStorage() string {
	defer s.TimeTrack(time.Now())
	return randItem([]string{
		"default",
		"vs1",
		"vs2",
		"vs3",
	})
}

func (s *Seed) fakeRelativePath() string {
	defer s.TimeTrack(time.Now())
	h := sha256.New()
	token := make([]byte, 4)
	rand.Read(token)
	h.Write(token)
	value := hex.EncodeToString(h.Sum(nil))

	prefixes := []string{"@cluster", "@pools", "@hashed"}

	val := fmt.Sprint(randItem(prefixes), "/", value[0:2], "/", value[2:4], "/", value, ".git")
	s.cacheRelativePath = append(s.cacheRelativePath, val)
	return val
}

func (s *Seed) fakeGeneration() *int {
	defer s.TimeTrack(time.Now())
	x := rand.Intn(100)
	if x == 0 {
		return nil
	}
	return &x
}

func (s *Seed) fakeReplicaPath() string {
	defer s.TimeTrack(time.Now())
	return s.fakeRelativePath()
}
