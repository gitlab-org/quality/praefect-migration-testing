package seeder

import (
	"math/rand"
	"time"
)

func (s *Seed) fakeTimeStamp() string {
	defer s.TimeTrack(time.Now())
	return s.fakeTimeStampBetween(time.Unix(0, 0), time.Now())
}

func (s *Seed) fakeTimeStampBetween(start, end time.Time) string {
	defer s.TimeTrack(time.Now())
	min := start.Unix()
	max := end.Unix()
	return time.Unix(rand.Int63n(max-min)+min, 0).Format(time.RFC3339)
}

func (s *Seed) fakeNodeName() string {
	defer s.TimeTrack(time.Now())
	return randItem([]string{
		"default", "node-1", "node-2", "node-3", "node-4", "node-5",
	})
}

func (s *Seed) fakeStorage(allowNil bool) *string {
	defer s.TimeTrack(time.Now())
	validValues := []*string{
		strPtr("storage-praefect-1"),
		strPtr("storage-praefect-2"),
		strPtr("storage-praefect-3"),
		nil,
	}
	if !allowNil {
		validValues = validValues[:len(validValues)-1]
	}
	return validValues[rand.Intn(len(validValues))]
}
