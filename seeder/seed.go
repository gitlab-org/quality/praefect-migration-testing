package seeder

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"github.com/jmoiron/sqlx"
	"math/rand"
	"regexp"
	"runtime"
	"time"
)

type Seed struct {
	DB            *sqlx.DB
	TimeTrackData map[string]int64

	// indexes cache
	deleteReplicaUniqueIndexCache map[string]bool
	shardNodeNamesOnNodeStatusIdx map[string]bool
	storageRepositoriesPkey       map[string]bool

	cacheRelativePath []string
	cacheRepositories []*repostitory
}

func NewSeed(db *sqlx.DB) *Seed {
	var s Seed
	s.DB = db
	s.deleteReplicaUniqueIndexCache = make(map[string]bool)
	s.shardNodeNamesOnNodeStatusIdx = make(map[string]bool)
	s.storageRepositoriesPkey = make(map[string]bool)

	s.cacheRelativePath = make([]string, 0)
	s.cacheRepositories = make([]*repostitory, 0)
	s.TimeTrackData = make(map[string]int64)
	return &s
}

func strPtr(s string) *string {
	return &s
}

func randItem(items []string) string {
	return items[rand.Intn(len(items))]
}

func sha256Int(n int) string {
	h := sha256.New()
	token := []byte(fmt.Sprint(n))
	rand.Read(token)
	h.Write(token)
	return hex.EncodeToString(h.Sum(nil))
}

func (s *Seed) TimeTrack(start time.Time) {
	elapsed := time.Since(start).Nanoseconds()

	// Skip this function, and fetch the PC and file for its parent.
	pc, _, _, _ := runtime.Caller(1)

	// Retrieve a function object this functions parent.
	funcObj := runtime.FuncForPC(pc)

	// Regex to extract just the function name (and not the module path).
	runtimeFunc := regexp.MustCompile(`^.*\.(.*)$`)
	name := runtimeFunc.ReplaceAllString(funcObj.Name(), "$1")

	s.TimeTrackData[name] = s.TimeTrackData[name] + elapsed

	//log.Println(fmt.Sprintf("%s took %s", name, elapsed))
}
