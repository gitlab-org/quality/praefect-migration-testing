package seeder

import (
	"math/rand"
	"time"
)

func (s *Seed) StorageRepositories(n int) error {
	defer s.TimeTrack(time.Now())
	for i := 0; i < n; i++ {
		if i >= len(s.cacheRepositories) {
			return nil
		}

		r := s.cacheRepositories[i]
		storage := *s.fakeStorage(false)
		if _, found := s.storageRepositoriesPkey[r.VirtualStorage+r.RelativePath+storage]; found {
			continue
		}
		s.storageRepositoriesPkey[r.VirtualStorage+r.RelativePath+r.VirtualStorage] = true

		tx := s.DB.MustBegin()
		tx.MustExec(`
		INSERT INTO public.storage_repositories (virtual_storage, relative_path, storage, generation) 
		VALUES ($1, $2, $3, $4)
		`,
			r.VirtualStorage,
			r.RelativePath,
			storage,
			rand.Intn(1_000),
		)
		if err := tx.Commit(); err != nil {
			panic(err)
		}
	}
	return nil
}
