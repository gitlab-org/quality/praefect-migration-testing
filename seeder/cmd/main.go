package main

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"math/rand"
	"os"
	"seeder"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func ensureEnv() {
	if _, set := os.LookupEnv("GITLAB_QA_GITALY_SEEDER_DB_USER"); !set {
		//hint format for GDK is probably your username `whoami`
		panic("Didn't find env GITLAB_QA_GITALY_SEEDER_DB_USER")
	}
	if _, set := os.LookupEnv("GITLAB_QA_GITALY_SEEDER_INSTANCE_UNIX_SOCKET"); !set {
		// hint format for GDK is  '~/dev/gdk/postgresql'
		panic("Didn't find env GITLAB_QA_GITALY_SEEDER_INSTANCE_UNIX_SOCKET")
	}
	if _, set := os.LookupEnv("GITLAB_QA_GITALY_SEEDER_DB_NAME"); !set {
		//hint format for GDK is 'praefect_development'
		panic("Didn't find env GITLAB_QA_GITALY_SEEDER_DB_NAME")
	}

}

func main() {
	ensureEnv()
	start := time.Now()

	db, _ := connectUnixSocket()
	s := seeder.NewSeed(db)

	_ = s.Repositories(100_000)
	_ = s.ReplicationQueue(100_000)
	_ = s.NodeStatus(100_000)
	_ = s.StorageRepositories(100_000)
	_ = s.RepositoryAssignments(100_000)
	end := time.Now()

	fmt.Println("Total Duration:", end.Sub(start))
	for k, v := range s.TimeTrackData {
		fmt.Println("\t", k, time.Duration(v))
	}
}

func connectUnixSocket() (*sqlx.DB, error) {
	dbURI := fmt.Sprintf(
		"user=%s database=%s host=%s",
		os.Getenv("GITLAB_QA_GITALY_SEEDER_DB_USER"),
		os.Getenv("GITLAB_QA_GITALY_SEEDER_DB_NAME"),
		os.Getenv("GITLAB_QA_GITALY_SEEDER_INSTANCE_UNIX_SOCKET"),
	)
	return sqlx.Open("postgres", dbURI)
}
