package seeder

import (
	"encoding/json"
	"github.com/google/uuid"
	"math/rand"
	"time"
)

func (s *Seed) ReplicationQueue(n int) error {
	defer s.TimeTrack(time.Now())
	tx := s.DB.MustBegin()
	for i := 0; i < n; i++ {
		tx.MustExec(`
		INSERT INTO replication_queue (state, created_at, updated_at, attempt, lock_id, job, meta) 
		VALUES ($1, $2, $3, $4, $5, $6, $7)`,
			s.fakeState(),
			s.fakeTimeStamp(),
			s.fakeTimeStamp(),
			rand.Intn(1000),
			s.fakeLockID(),
			s.fakeReplicationJob(),
			s.fakeMeta(),
		)
	}
	return tx.Commit()
}

func (s *Seed) fakeState() string {
	defer s.TimeTrack(time.Now())
	return randItem([]string{"ready", "in_progress", "completed", "cancelled", "failed", "dead"})
}

func (s *Seed) fakeLockID() string {
	defer s.TimeTrack(time.Now())

	return uuid.New().String()
}

func (s *Seed) fakeReplicationJob() *string {
	defer s.TimeTrack(time.Now())
	// some nil values
	if rand.Intn(10)%10 == 0 {
		return nil
	}

	var j ReplicationJob
	j.Change = ChangeType(randItem([]string{
		"update",
		"create",
		"delete",
		"delete_replica",
		"rename",
		"gc",
		"repack_full",
		"repack_incremental",
		"cleanup",
		"pack_refs",
	}))

	j.RelativePath = randItem(s.cacheRelativePath)
	j.VirtualStorage = s.fakeVirtualStorage()

	//avoiding duplicate key constraint "delete_replica_unique_index"
	if _, found := s.deleteReplicaUniqueIndexCache[j.RelativePath+j.VirtualStorage]; found {
		return nil
	}
	s.deleteReplicaUniqueIndexCache[j.RelativePath+j.VirtualStorage] = true

	j.TargetNodeStorage = *s.fakeStorage(false)
	j.SourceNodeStorage = *s.fakeStorage(false)

	j.Params = Params{"correlation_id": rand.Int63(), uuid.New().String(): rand.Int63()}

	data, _ := json.Marshal(j)
	return strPtr(string(data))
}

func (s *Seed) fakeMeta() *string {
	defer s.TimeTrack(time.Now())

	if rand.Intn(10)%10 == 0 {
		return nil
	}
	p := &Params{
		uuid.New().String(): uuid.New().String(),
		uuid.New().String(): rand.Int63(),
	}

	data, _ := json.Marshal(p)
	return strPtr(string(data))
}

type ReplicationJob struct {
	Change            ChangeType `json:"change"`
	RelativePath      string     `json:"relative_path"`
	TargetNodeStorage string     `json:"target_node_storage"`
	SourceNodeStorage string     `json:"source_node_storage"`
	VirtualStorage    string     `json:"virtual_storage"`
	Params            Params     `json:"params"`
}

type Params map[string]interface{}
type ChangeType string
