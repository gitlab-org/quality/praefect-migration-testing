package seeder

import (
	"time"
)

func (s *Seed) RepositoryAssignments(n int) error {
	defer s.TimeTrack(time.Now())
	for i := 0; i < n; i++ {
		if i >= len(s.cacheRepositories) {
			return nil
		}

		r := s.cacheRepositories[i]
		storage := *s.fakeStorage(false)
		if _, found := s.storageRepositoriesPkey[r.VirtualStorage+r.RelativePath+storage]; found {
			continue
		}
		s.storageRepositoriesPkey[r.VirtualStorage+r.RelativePath+r.VirtualStorage] = true

		tx := s.DB.MustBegin()
		tx.MustExec(`
		INSERT INTO public.repository_assignments (virtual_storage, relative_path, storage) 
		VALUES ($1, $2, $3)
		`,
			r.VirtualStorage,
			r.RelativePath,
			storage,
		)
		if err := tx.Commit(); err != nil {
			panic(err)
		}
	}
	return nil
}
