#!/bin/bash

set -eux

pg_restore --no-owner --dbname praefect_migration_test -v "/data/database.tar.gz"

echo -n "Estimated row count"
psql -U postgres -d praefect_migration_test -c 'SELECT schemaname,relname,n_live_tup FROM pg_stat_user_tables ORDER BY n_live_tup DESC;'