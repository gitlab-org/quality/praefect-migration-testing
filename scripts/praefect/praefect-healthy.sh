#!/bin/bash

set -ex

# This will return an empty string until such time praefect service is running
gitlab-ctl status praefect | grep -q 'run: praefect:';

# Checking the service status
str=`praefect -config /var/opt/gitlab/praefect/config.toml sql-migrate-status `
echo $str
if [[ $str =~ ' no ' ]]; then
    exit 1
fi

# Checking the migration status
if [[ `praefect -config /var/opt/gitlab/praefect/config.toml sql-migrate-status ` =~ ' no ' ]]; then
    exit 1
fi
