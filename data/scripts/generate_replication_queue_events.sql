-- To disable triggers set the database to a replica, reset it after seeding data.
SET session_replication_role = replica;
-- SET session_replication_role = DEFAULT;

INSERT INTO public.repositories(
 	virtual_storage, relative_path, generation, "primary", replica_path)
SELECT 
	virtual_storage as "virtual_storage",
	repo_path as "relative_path",
    (CASE WHEN ceil(random()*100)::integer % 7=0 THEN NULL ELSE ceil(random() * 5000)::integer END) as "generation",
	(CASE WHEN ceil(random()*100)::integer % 7=0 THEN NULL ELSE praefect_storage END) as "primary",
	(CASE WHEN ceil(random()*100)::integer % 7=0 THEN NULL ELSE repo_path END) as "replica_path"
FROM (
	SELECT
		praefect_storage,
		virtual_storage,
		concat('@hashed/', substr(md5(random()::text), 0, 3), '/', substr(md5(random()::text), 3, 2), '/', substr(md5(random()::text), 0, 64), '.git') as "repo_path"
	FROM (VALUES (md5(random()::text)), (md5(random()::text)), (md5(random()::text))) AS praefect_storage(praefect_storage)
	CROSS JOIN (VALUES ('default'), ('default'), ('default'), ('default'), (md5(random()::text))) AS virtual_storage(virtual_storage)
	CROSS JOIN generate_series(1, 1000000)	--- SET TO TARGET NUMBER ROWS
	ORDER BY random()
	LIMIT 1000000	--- SET TO TARGET NUMBER ROWS
) as repositories_data;


INSERT INTO public.storage_repositories(
	"storage", virtual_storage, relative_path , generation, repository_id)
SELECT 
	primaries."primary" as "storage",
	virtual_storage,
	relative_path,
	generation,
	repository_id
FROM repositories r
CROSS JOIN 
	(SELECT DISTINCT r2."primary" FROM repositories r2 WHERE "primary" IS NOT NULL) as primaries
WHERE generation IS NOT NULL
AND r."primary" IS NOT NULL
LIMIT 1000000
;



-- Turn triggers back on now that data is seeded
-- SET session_replication_role = replica;
SET session_replication_role = DEFAULT;




INSERT INTO repository_assignments
SELECT virtual_storage, relative_path, "primary", repository_id FROM repositories WHERE "primary" IS NOT NULL;



INSERT INTO public.replication_queue("id", "state", created_at, updated_at, attempt)
	SELECT 
		repository_id,
		(SELECT unnest(enum_range(NULL::replication_job_state)) as "state" LIMIT 1),
		NOW() AS created_at,
		NOW() AS updated_at,
		CEIL(RANDOM()*10) AS attempt
	FROM storage_repositories
	WHERE repository_id IS NOT NULL
ON CONFLICT (id) DO NOTHING;



UPDATE public.replication_queue
SET state=(
	CASE CEIL(RANDOM()*6)
		WHEN 1 THEN (SELECT elem FROM unnest(enum_range(NULL::replication_job_state)) WITH ORDINALITY AS a(elem, nr) WHERE nr=1)
		WHEN 2 THEN (SELECT elem FROM unnest(enum_range(NULL::replication_job_state)) WITH ORDINALITY AS a(elem, nr) WHERE nr=2)
		WHEN 3 THEN (SELECT elem FROM unnest(enum_range(NULL::replication_job_state)) WITH ORDINALITY AS a(elem, nr) WHERE nr=3)
		WHEN 4 THEN (SELECT elem FROM unnest(enum_range(NULL::replication_job_state)) WITH ORDINALITY AS a(elem, nr) WHERE nr=4)
		WHEN 5 THEN (SELECT elem FROM unnest(enum_range(NULL::replication_job_state)) WITH ORDINALITY AS a(elem, nr) WHERE nr=5)
		WHEN 6 THEN (SELECT elem FROM unnest(enum_range(NULL::replication_job_state)) WITH ORDINALITY AS a(elem, nr) WHERE nr=6)
	END);



UPDATE public.replication_queue rq
SET lock_id = concat(sr.virtual_storage,'|',sr.storage,'|',sr.relative_path)
FROM public.storage_repositories sr
WHERE rq.id = sr.repository_id
;


UPDATE public.replication_queue rq
SET job =  concat('{
	"change": "',
		CASE CEIL(RANDOM()*14)
			WHEN 1 THEN 'update'
			WHEN 2 THEN 'create'
			WHEN 3 THEN 'delete'
			WHEN 4 THEN 'delete_replica'
			WHEN 5 THEN 'rename'
			WHEN 6 THEN 'gc'
			WHEN 7 THEN 'repack_full'
			WHEN 8 THEN 'repack_incremental'
			WHEN 9 THEN 'cleanup'
			WHEN 10 THEN 'pack_refs'
			WHEN 11 THEN 'write_commit_graph'
			WHEN 12 THEN 'midx_repack'
			WHEN 13 THEN 'optimize_repository'
			ELSE NULL
		END
	,'",
	"relative_path": "',sr.relative_path,'",
	"repository_id": ',sr.repository_id,',
	"virtual_storage": "',sr.virtual_storage,'",
	"source_node_storage": "',sr.storage,'",
	"target_node_storage": "',sr.storage,'"
	}')::json
FROM public.storage_repositories sr
WHERE rq.id = sr.repository_id;


UPDATE public.replication_queue
SET meta = concat('{"correlation_id": "',md5(random()::text),'"}')::json;



SELECT * FROM replication_queue;
