-- To disable triggers set the database to a replica, reset it after seeding data.
SET session_replication_role = replica;
-- SET session_replication_role = DEFAULT;

INSERT INTO public.repositories(
 	virtual_storage, relative_path, generation, "primary", replica_path)
SELECT 
	virtual_storage as "virtual_storage",
	repo_path as "relative_path",
    (CASE WHEN ceil(random()*100)::integer % 7=0 THEN NULL ELSE ceil(random() * 5000)::integer END) as "generation",
	(CASE WHEN ceil(random()*100)::integer % 7=0 THEN NULL ELSE praefect_storage END) as "primary",
	(CASE WHEN ceil(random()*100)::integer % 7=0 THEN NULL ELSE repo_path END) as "replica_path"
FROM (
	SELECT
		praefect_storage,
		virtual_storage,
		concat('@hashed/', substr(md5(random()::text), 0, 3), '/', substr(md5(random()::text), 3, 2), '/', substr(md5(random()::text), 0, 64), '.git') as "repo_path"
	FROM (VALUES (md5(random()::text)), (md5(random()::text)), (md5(random()::text))) AS praefect_storage(praefect_storage)
	CROSS JOIN (VALUES ('default'), ('default'), ('default'), ('default'), (md5(random()::text))) AS virtual_storage(virtual_storage)
	CROSS JOIN generate_series(1, 100000)	--- SET TO TARGET NUMBER ROWS
	ORDER BY random()
	LIMIT 100000	--- SET TO TARGET NUMBER ROWS
) as repositories_data;


INSERT INTO public.storage_repositories(
	"storage", virtual_storage, relative_path , generation, repository_id)
SELECT 
	primaries."primary" as "storage",
	virtual_storage,
	relative_path,
	generation,
	(CASE WHEN repository_id % 7=0 THEN repository_id ELSE NULL END)	-- 6/7 will have a NULL repository_id
FROM repositories r
CROSS JOIN 
	(SELECT DISTINCT r2."primary" FROM repositories r2 WHERE "primary" IS NOT NULL) as primaries
WHERE generation IS NOT NULL
AND r."primary" IS NOT NULL;


-- Validate the dataset is as expected
SELECT * FROM public.repositories;
SELECT * FROM public.storage_repositories;

-- Turn triggers back on now that data is seeded
-- SET session_replication_role = replica;
SET session_replication_role = DEFAULT;